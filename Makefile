# Copyright (c) 2015-2019 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL Samuel Thibault BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
# OF SUCH DAMAGE.

ALL=look
CFLAGS += -g -Wall
CXXFLAGS += -g -Wall

all: build

GTK2_BUILT=$(realpath show_gtk2)
GTK3_BUILT=$(realpath show_gtk3)
GTK4_BUILT=$(realpath show_gtk4)
QT4_BUILT=$(realpath show_qt4)
QT5_BUILT=$(realpath show_qt5)
JAVA_BUILT=$(realpath show_java.class)
GTKSHARP2_BUILT=$(realpath show_gtksharp2)
GTKSHARP3_BUILT=$(realpath show_gtksharp3)
WINFORMS_BUILT=$(realpath show_winforms)

ALL_BUILT=
ifneq ($(GTK2_BUILT),)
ifneq ($(GTK3_BUILT),)
ifneq ($(QT4_BUILT),)
ifneq ($(QT5_BUILT),)
ifneq ($(JAVA_BUILT),)
ifneq ($(GTKSHARP2_BUILT),)
ifneq ($(GTKSHARP3_BUILT),)
ifneq ($(WINFORMS_BUILT),)
ALL_BUILT=yes
endif
endif
endif
endif
endif
endif
endif
endif

#
# Check for dbus & atspi2
#
ifeq ($(shell pkg-config --libs dbus-1 2> /dev/null)$(ALL_BUILT),)
  $(error please install pkg-config libdbus-1-dev)
endif

ifeq ($(shell pkg-config --libs atspi-2 2> /dev/null)$(ALL_BUILT),)
  $(error please install pkg-config libatspi2.0-dev)
endif

CPPFLAGS  += $(shell pkg-config --cflags dbus-1 2> /dev/null)
LDFLAGS   += $(shell pkg-config --libs-only-L dbus-1 2> /dev/null)
LDLIBS    += $(shell pkg-config --libs-only-l dbus-1 2> /dev/null)

CPPFLAGS  += $(shell pkg-config --cflags atspi-2 2> /dev/null)
LDFLAGS   += $(shell pkg-config --libs-only-L atspi-2 2> /dev/null)
LDLIBS    += $(shell pkg-config --libs-only-l atspi-2 2> /dev/null)


#
# Check for GTK 2
#
ifneq ($(shell pkg-config --libs gtk+-2.0 2> /dev/null),)
GTK2=Y
show_gtk2.c: show_gtk.c
	ln -f $< $@
show_gtk2.o: CPPFLAGS+=$(shell pkg-config --cflags gtk+-2.0)
show_gtk2: show_gtk2.o
show_gtk2: LDFLAGS+=$(shell pkg-config --libs-only-L gtk+-2.0)
show_gtk2: LDLIBS+=$(shell pkg-config --libs-only-l gtk+-2.0)
ALL+=show_gtk2
endif


#
# Check for GTK 3
#
ifneq ($(shell pkg-config --libs gtk+-3.0 2> /dev/null),)
GTK3=Y
show_gtk3.c: show_gtk.c
	ln -f $< $@
show_gtk3.o: CPPFLAGS+=$(shell pkg-config --cflags gtk+-3.0)
show_gtk3: show_gtk3.o
show_gtk3: LDFLAGS+=$(shell pkg-config --libs-only-L gtk+-3.0)
show_gtk3: LDLIBS+=$(shell pkg-config --libs-only-l gtk+-3.0)
ALL+=show_gtk3
endif


#
# Check for GTK 4
#
ifneq ($(shell pkg-config --libs gtk+-4.0 2> /dev/null),)
GTK4=Y
show_gtk4.c: show_gtk.c
	ln -f $< $@
show_gtk4.o: CPPFLAGS+=$(shell pkg-config --cflags gtk+-4.0)
show_gtk4: show_gtk4.o
show_gtk4: LDFLAGS+=$(shell pkg-config --libs-only-L gtk+-4.0)
show_gtk4: LDLIBS+=$(shell pkg-config --libs-only-l gtk+-4.0)
ALL+=show_gtk4
endif


#
# Check for PyGtk 2
#
ifeq ($(shell python -c 'import pygtk ; import gtk' > /dev/null 2>&1)$(.SHELLSTATUS),0)
PYGTK2=Y
endif


#
# Check for Python Gtk 3
#
ifeq ($(shell python3 -c "import gi ; gi.require_version('Gtk', '3.0') ; from gi.repository import Gtk" > /dev/null 2>&1)$(.SHELLSTATUS),0)
PYGTK3=Y
endif


#
# Check for Qt 4
#
ifneq ($(shell pkg-config --libs QtGui 2> /dev/null),)
QT4=Y
show_qt4.cpp: show_qt.cpp
	ln $< $@
show_qt4.o: CPPFLAGS+=$(shell pkg-config --cflags QtGui)
show_qt4: show_qt4.o
show_qt4: LDFLAGS+=$(shell pkg-config --libs-only-L QtGui)
show_qt4: LDLIBS+=$(shell pkg-config --libs-only-l QtGui) -lstdc++
ALL+=show_qt4
endif


#
# Check for Qt 5
#
ifneq ($(shell pkg-config --libs Qt5Widgets 2> /dev/null),)
QT5=Y
show_qt5.cpp: show_qt.cpp
	ln $< $@
show_qt5.o: CPPFLAGS+=$(shell pkg-config --cflags Qt5Widgets)
show_qt5.o: CXXFLAGS+=-fPIC
show_qt5: show_qt5.o
show_qt5: LDFLAGS+=$(shell pkg-config --libs-only-L Qt5Widgets)
show_qt5: LDLIBS+=$(shell pkg-config --libs-only-l Qt5Widgets) -lstdc++
ALL+=show_qt5
endif


#
# Check for PyQt4
#
ifeq ($(shell python3 -c 'from PyQt4 import QtGui' > /dev/null 2>&1)$(.SHELLSTATUS),0)
PYQT4=Y
endif


#
# Check for PyQt5
#
ifeq ($(shell python3 -c 'from PyQt4 import QtWidgets' > /dev/null 2>&1)$(.SHELLSTATUS),0)
PYQT5=Y
endif


#
# Check for java
#
ifneq (,$(wildcard $(shell which javac)))
ifneq (,$(wildcard $(shell which java)))
JAVA=Y
ALL+=show_java.class
endif
endif

%.class: %.java
	javac -source 1.7 -target 1.7 $<

ifneq (,$(wildcard /usr/lib/jvm/java-7-openjdk-amd64/bin/java))
JAVA7=Y
endif
ifneq (,$(wildcard /usr/lib/jvm/java-8-openjdk-amd64/bin/java))
JAVA8=Y
endif
ifneq (,$(wildcard /usr/lib/jvm/java-9-openjdk-amd64/bin/java))
JAVA9=Y
endif
ifneq (,$(wildcard /usr/lib/jvm/java-10-openjdk-amd64/bin/java))
JAVA10=Y
endif
ifneq (,$(wildcard /usr/lib/jvm/java-11-openjdk-amd64/bin/java))
JAVA11=Y
endif
ifneq (,$(wildcard /usr/lib/jvm/java-12-openjdk-amd64/bin/java))
JAVA12=Y
endif
ifneq (,$(wildcard /usr/lib/jvm/java-13-openjdk-amd64/bin/java))
JAVA13=Y
endif

#
# Check for mono
#
ifneq (,$(wildcard $(shell which mcs)))

#
# Check for GTK# 2
#
ifneq ($(shell pkg-config --libs gtk-sharp-2.0 2> /dev/null),)
GTKSHARP2=Y
show_gtksharp2: MCSFLAGS+=-pkg:gtk-sharp-2.0
show_gtksharp2.cs: show_gtksharp.cs
	ln $< $@
ALL+=show_gtksharp2
endif

%: %.cs
	mcs $(MCSFLAGS) $< -out:$@

#
# Check for GTK# 3
#
ifneq ($(shell pkg-config --libs gtk-sharp-3.0 2> /dev/null),)
GTKSHARP3=Y
show_gtksharp3: MCSFLAGS+=-pkg:gtk-sharp-3.0
show_gtksharp3.cs: show_gtksharp.cs
	ln $< $@
ALL+=show_gtksharp3
endif

WINFORMS=Y
show_winforms: MCSFLAGS+=-r:System.Windows.Forms.dll
ALL+=show_winforms

%: %.cs
	mcs $(MCSFLAGS) $< -out:$@

endif # mcs

#
# We want at least one
#
ifeq ($(GTK2)$(GTK3)$(GTK4)$(PYGTK2)$(PYGTK3)$(QT4)$(QT5)$(PYQT4)$(PYQT5)$(JAVA)$(GTKSHARP2)$(GTKSHARP3)$(WINFORMS)$(ALL_BUILT),)
  $(error Please install libgtk2.0-dev, libgtk-3-dev, libgtk-4-dev, python-gobject-2, gir1.2-gtk-3.0, libqt4-dev, qtbase5-dev, python3-pyqt4, python3-pyqt5, default-jre, mcs libgtk2.0-cil-dev, or mcs libgtk3.0-cil-dev, or mcs)
endif


#
# Build everything
#
build: $(ALL)


#
# And test everything
#
check_gtk2: show_gtk2 look
ifneq ($(GTK2)$(GTK2_BUILT),)
	./check gtk2
endif

check_gtk3: show_gtk3 look
ifneq ($(GTK3)$(GTK3_BUILT),)
	./check gtk3
endif

check_gtk4: show_gtk4 look
ifneq ($(GTK4)$(GTK4_BUILT),)
	./check gtk4
endif

check_pygtk2: look
ifneq ($(PYGTK2),)
	./check pygtk2
endif

check_pygtk3: look
ifneq ($(PYGTK3),)
	./check pygtk3
endif

check_qt4: show_qt4 look
ifneq ($(QT4)$(QT4_BUILT),)
	./check qt4
endif

check_qt5: show_qt5 look
ifneq ($(QT5)$(QT5_BUILT),)
	./check qt5
endif

check_pyqt4: look
ifneq ($(PYQT4),)
	./check pyqt4
endif

check_pyqt4msg: look
ifneq ($(PYQT4),)
	./check pyqt4msg
endif

check_pyqt5msg: look
ifneq ($(PYQT5),)
	./check pyqt5msg
endif

check_pyqt5: look
ifneq ($(PYQT5),)
	./check pyqt5
endif

check_java: show_java.class look
ifneq ($(JAVA)$(JAVA_BUILT),)
	./check java
endif

check_java7: show_java.class look
ifneq ($(JAVA7)$(JAVA_BUILT),)
	./check java7
endif
check_java8: show_java.class look
ifneq ($(JAVA8)$(JAVA_BUILT),)
	./check java8
endif
check_java9: show_java.class look
ifneq ($(JAVA9)$(JAVA_BUILT),)
	./check java9
endif
check_java10: show_java.class look
ifneq ($(JAVA10)$(JAVA_BUILT),)
	./check java10
endif
check_java11: show_java.class look
ifneq ($(JAVA11)$(JAVA_BUILT),)
	./check java11
endif
check_java12: show_java.class look
ifneq ($(JAVA12)$(JAVA_BUILT),)
	./check java12
endif
check_java13: show_java.class look
ifneq ($(JAVA13)$(JAVA_BUILT),)
	./check java13
endif

check_gtksharp2: show_gtksharp2 look
ifneq ($(GTKSHARP2)$(GTKSHARP2_BUILT),)
	./check gtksharp2
endif

check_gtksharp3: show_gtksharp3 look
ifneq ($(GTKSHARP3)$(GTKSHARP3_BUILT),)
	./check gtksharp3
endif

check_winforms: show_winforms look
ifneq ($(WINFORMS)$(WINFORMS_BUILT),)
	./check winforms
endif

# check_gtksharp2 check_gtksharp3 disabled for now, needs a way to detect whether we can run it
# check_winforms disabled for now, as it doesn't work.
# check_java9 and check_java10 disabled as they are not fixed (see #900912)
check: check_gtk2 check_gtk3 check_gtk4 check_pygtk2 check_pygtk3 check_qt4 check_qt5 check_pyqt4 check_pyqt4msg check_pyqt5 check_pyqt5msg check_java check_java7 check_java8 check_java11 check_java12 check_java13

#
# And clean our mess
#
clean:
	rm -f $(ALL) *.o show_gtk2.c show_gtk3.c show_gtk4.c show_qt4.cpp show_qt5.cpp
