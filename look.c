/*

Copyright (c) 2015-2016, 2018 Samuel Thibault <samuel.thibault@ens-lyon.org>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
EVENT SHALL Samuel Thibault BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <dbus/dbus.h>
#include <atspi/atspi.h>

#if 0
#define PRINTF(fmt, ...) (void)0
#else
#define PRINTF(fmt, ...) printf(fmt, ##__VA_ARGS__)
#endif

#define SPI2_DBUS_INTERFACE		"org.a11y.atspi"
#define SPI2_DBUS_INTERFACE_REG		SPI2_DBUS_INTERFACE".Registry"
#define SPI2_DBUS_INTERFACE_ACCESSIBLE	SPI2_DBUS_INTERFACE".Accessible"
#define SPI2_DBUS_INTERFACE_COMPONENT	SPI2_DBUS_INTERFACE".Component"
#define SPI2_DBUS_PATH_ROOT		"/org/a11y/atspi/accessible/root"
#define SPI2_DBUS_INTERFACE_PROP	"org.freedesktop.DBus.Properties"

static DBusConnection *bus = NULL;
static DBusError error;

/* Creates a method call message */
static DBusMessage *
new_method_call(const char *sender, const char *path, const char *interface, const char *method)
{
  DBusMessage *msg;

  msg = dbus_message_new_method_call(sender, path, interface, method);
  if (dbus_error_is_set(&error)) {
    fprintf(stderr, "error while making %s message: %s %s", method, error.name, error.message);
    dbus_error_free(&error);
    return NULL;
  }
  if (!msg) {
    fprintf(stderr, "no memory while making %s message", method);
    return NULL;
  }
  return msg;
}

/* Sends a method call message, and returns the reply, if any. This unrefs the message.  */
static DBusMessage *
send_with_reply_and_block(DBusConnection *bus, DBusMessage *msg, int timeout_ms, const char *doing)
{
  DBusMessage *reply;

  reply = dbus_connection_send_with_reply_and_block(bus, msg, timeout_ms, &error);
  dbus_message_unref(msg);
  if (dbus_error_is_set(&error)) {
    fprintf(stderr, "error while %s: %s %s\n", doing, error.name, error.message);
    dbus_error_free(&error);
    return NULL;
  }
  if (!reply) {
    fprintf(stderr, "timeout while %s\n", doing);
    return NULL;
  }
  if (dbus_message_get_type (reply) == DBUS_MESSAGE_TYPE_ERROR) {
    fprintf(stderr, "error while %s\n", doing);
    dbus_message_unref(reply);
    return NULL;
  }
  return reply;
}

void check_prop(const char *sender, const char *path)
{
  const char *res;
  DBusMessage *msg, *reply;
  const char *interface = SPI2_DBUS_INTERFACE_ACCESSIBLE;
  const char *property = "Name";
  DBusMessageIter iter, iter_variant;

  msg = new_method_call(sender, path, SPI2_DBUS_INTERFACE_PROP, "Get");
  if (!msg)
  {
    fprintf(stderr,"can't create method call Get?!\n");
    exit(EXIT_FAILURE);
  }
  dbus_message_append_args(msg, DBUS_TYPE_STRING, &interface, DBUS_TYPE_STRING, &property, DBUS_TYPE_INVALID);
  reply = send_with_reply_and_block(bus, msg, 1000, "getting property");
  if (!reply)
  {
    fprintf(stderr,"No reply for Getting obj properties\n");
    exit(EXIT_FAILURE);
  }

  dbus_message_iter_init(reply, &iter);
  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_VARIANT) {
    fprintf(stderr, "get didn't return a variant but '%c'\n", dbus_message_iter_get_arg_type(&iter));
    goto out;
  }
  dbus_message_iter_recurse(&iter, &iter_variant);
  if (dbus_message_iter_get_arg_type(&iter_variant) != DBUS_TYPE_STRING) {
    fprintf(stderr, "get didn't return a string but '%c'\n", dbus_message_iter_get_arg_type(&iter_variant));
    goto out;
  }
  dbus_message_iter_get_basic(&iter_variant, &res);
  PRINTF("    Got name '%s'\n", res);
  if (!strcmp(res, "check-a11y"))
  {
    /* Success! */
    printf("Success\n");
    exit(EXIT_SUCCESS);
  }

out:
  dbus_message_unref(reply);
}

void check_show(const char *sender, const char *path)
{
  DBusMessage *msg, *reply;
  DBusMessageIter iter, iter_array, iter_struct;
  /*
   * Get list of toplevel widgets
   */
  msg = new_method_call(sender, path, SPI2_DBUS_INTERFACE_ACCESSIBLE, "GetChildren");
  if (!msg)
  {
    fprintf(stderr,"can't create method call GetChildren?!\n");
    exit(EXIT_FAILURE);
  }
  reply = send_with_reply_and_block(bus, msg, 1000, "getting children");

  if (!reply)
  {
    fprintf(stderr,"no answer from %s %s\n", sender, path);
    return;
  }

  if (strcmp(dbus_message_get_signature(reply), "a(so)") != 0)
  {
    fprintf(stderr,"unexpected signature %s while getting children\n", dbus_message_get_signature(reply));
    exit(EXIT_FAILURE);
  }
  dbus_message_iter_init(reply, &iter);
  dbus_message_iter_recurse(&iter, &iter_array);
  while (dbus_message_iter_get_arg_type(&iter_array) != DBUS_TYPE_INVALID)
  {
    const char *sender, *path;
    dbus_message_iter_recurse(&iter_array, &iter_struct);
    dbus_message_iter_get_basic(&iter_struct, &sender);
    dbus_message_iter_next(&iter_struct);
    dbus_message_iter_get_basic(&iter_struct, &path);
    PRINTF("  got %s at %s\n", sender, path);
    check_prop(sender, path);
    dbus_message_iter_next(&iter_array);
  }
  PRINTF("  end of list\n");
  dbus_message_unref(reply);
}

int main(void) {
  DBusMessage *msg, *reply;
  DBusMessageIter iter, iter_array, iter_struct;
  int tries = 5;

  /*
   * Connect to bus
   */
  dbus_error_init(&error);
  bus = atspi_get_a11y_bus();
  if (!bus)
  {
    bus = dbus_bus_get(DBUS_BUS_SESSION, &error);
    if (dbus_error_is_set(&error))
    {
      fprintf(stderr, "Can't get dbus session bus: %s %s\n", error.name, error.message);
      exit(EXIT_FAILURE);
    }
  }
  if (!bus)
  {
    fprintf(stderr, "Can't get dbus session bus.\n");
    exit(EXIT_FAILURE);
  }

  /*
   * Get list of applications
   */
  while(--tries)
  {
    msg = new_method_call(SPI2_DBUS_INTERFACE_REG, SPI2_DBUS_PATH_ROOT, SPI2_DBUS_INTERFACE_ACCESSIBLE, "GetChildren");
    if (!msg)
    {
      fprintf(stderr,"can't create method call GetChildren?!\n");
      exit(EXIT_FAILURE);
    }
    reply = send_with_reply_and_block(bus, msg, 1000, "getting children");

    if (strcmp(dbus_message_get_signature(reply), "a(so)") != 0)
    {
      fprintf(stderr,"unexpected signature %s while getting children\n", dbus_message_get_signature(reply));
      exit(EXIT_FAILURE);
    }
    dbus_message_iter_init(reply, &iter);
    dbus_message_iter_recurse(&iter, &iter_array);
    while (dbus_message_iter_get_arg_type(&iter_array) != DBUS_TYPE_INVALID)
    {
      const char *sender, *path;
      dbus_message_iter_recurse(&iter_array, &iter_struct);
      dbus_message_iter_get_basic(&iter_struct, &sender);
      dbus_message_iter_next(&iter_struct);
      dbus_message_iter_get_basic(&iter_struct, &path);
      PRINTF("got %s at %s\n", sender, path);
      check_show(sender, path);
      dbus_message_iter_next(&iter_array);
    }
    PRINTF("end of list\n");
    dbus_message_unref(reply);

    PRINTF("didn't find our check-a11y application, waiting a bit\n");
    sleep(1);
    PRINTF("\n");
  }
  printf("Failed\n");

  dbus_connection_close(bus);
  exit(EXIT_FAILURE);
}
