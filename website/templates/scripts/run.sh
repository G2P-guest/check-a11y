#!/bin/sh -ex

if [ -f /overrides/pre ]; then
. /overrides/pre
fi


#get fresh checkout of check-a11y, compile it

echo 
cd /root/check-a11y
git pull
make clean
make

#make sure /logs is cleaned

rm -rf /logs

#create logdirs, needed for Xvfb framebuffer and stderr/stdout
mkdir -p /logs/fb

apt show $2 > /logs/apt_show.txt

dpkg -l > /logs/dpkg.txt

export >/logs/export_before_x.txt

#get startup command from parameter

DF="/usr/share/xsessions/"$1

cmdline=$(cat $DF |grep -E "^\s*TryExec\s*=\s*" |cut -d "=" -f 2)

if [ -z "$cmdline" ]; then
cmdline=$(cat $DF |grep -E "^\s*Exec\s*=\s*" |cut -d "=" -f 2)
fi

echo found $cmdline

if [ -n "$CMDLINE" ] ; then
cmdline=$CMDLINE
fi


export XINITRC=/etc/X11/xinit/xinitrc

timeout -s9 40 xinit $cmdline -- /usr/bin/Xvfb :1 -screen 0 1600x1200x24 -fbdir /logs/fb > /logs/xinit_log.stdout 2> /logs/xinit_log.stderr &
mypid=$$



echo "pid: $mypid"
echo "waiting..."
sleep 15
export >/logs/export_after_xvfb_start.txt
ps auxww > /logs/ps_auxww_after_xvfb_start.txt


#create "initial" screenshot, without any toolkit checks
xwdtopnm /logs/fb/Xvfb_screen0 > /logs/fb/Xvfb_screen0.ppm
convert /logs/fb/Xvfb_screen0.ppm /logs/screenshot.png


cd /root/check-a11y
. ./env.sh
export DISPLAY=:1
make clean
timeout -s9 120 make -k check || true

timeout -s9 30 ./troubleshoot > /logs/troubleshoot.stdout.txt 2> /logs/troubleshoot.stderr.txt ||true

export >/logs/export_after_checks.txt


