#!/bin/sh -x

if [ "$USER" != "root" ]; then
    echo "Must be root to do that" 
    exit 1
fi


. ./config

run-parts --exit-on-error ./tasks


